%% ========================================================================== %%
%%  momojoba.cls v3.0 - class for job applications                            %%
%%  Copyright (c) 2021 Moritz Burmeister                                      %%
%%                                                                            %%
%%  This work may be distributed and/or modified under the                    %%
%%  conditions of the LaTeX Project Public License, either version 1.3        %%
%%  of this license or (at your option) any later version.                    %%
%%  The latest version of this license is in                                  %%
%%    http://www.latex-project.org/lppl.txt                                   %%
%%                                                                            %%
%%  This work has the LPPL maintenance status 'maintained'.                   %%
%%  The Current Maintainer of this work is Moritz Burmeister.                 %%
%%  ------------------------------------------------------------------------  %%
%%  This work is part of the 'mbltx' bundle, which can be found at:           %%
%%    https://gitlab.com/Itezos/mbltx                                         %%
%% ========================================================================== %%
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{momojoba}[2021-03-21 v3.0 class for job applications]
%% > Packages =============================================================== %%
\RequirePackage{kvoptions}            % key=value optionen definieren
\RequirePackage{etoolbox}
\RequirePackage{xparse}
\RequirePackage{expl3}

%% > Options ================================================================ %%
%% >> basic options --------------------------------------------------------  %%
\SetupKeyvalOptions{family=joba, prefix=joba@}

\DeclareStringOption{type}
\DeclareStringOption[Author Name]{author}
\DeclareStringOption[english]{language}

\ProcessKeyvalOptions{joba}

%% >> color options --------------------------------------------------------  %%
\SetupKeyvalOptions{family=jobacolor, prefix=jobacolor@}

\DeclareStringOption[000000]{textcolor}
\DeclareStringOption[000000]{highlightcolor}
\DeclareStringOption[ffffff]{shadecolor}
\DeclareStringOption[000000]{linkcolor}
\DeclareStringOption[000000]{skillcolor}

\ProcessKeyvalOptions{jobacolor}

%% >> contact infos --------------------------------------------------------  %%
\SetupKeyvalOptions{family=jobacontact, prefix=jobacontact@}

\DeclareStringOption[Authorstreet]{street}
\DeclareStringOption[0]{streetnr}
\DeclareStringOption[00000]{postalcode}
\DeclareStringOption[Authorcity]{city}
\DeclareStringOption[{+00(0)000-000-0000}]{phone}
\DeclareStringOption[foo.bar@baz.com]{email}
\DeclareStringOption{fulladdress}
\DeclareStringOption{homepage}
\DeclareStringOption{linkedin}
\DeclareStringOption{xing}
\DeclareStringOption{homepageshort}
\DeclareStringOption{linkedinshort}
\DeclareStringOption{xingshort}

\ProcessKeyvalOptions{jobacontact}

%% >> cover letter options -------------------------------------------------  %%
\SetupKeyvalOptions{family=jobacovl, prefix=jobacovl@}

\DeclareStringOption{covltitle}
\DeclareStringOption{covlsubject}
\DeclareStringOption[\today]{covldate}
\DeclareStringOption[The Company inc.]{recipientcompany}
\DeclareStringOption[Jane Doe]{recipientperson}
\DeclareStringOption[Companystreet]{recipientstreet}
\DeclareStringOption[0]{recipientstreetnr}
\DeclareStringOption[00000]{recipientpostalcode}
\DeclareStringOption[Companycity]{recipientcity}
\DeclareStringOption[Dear Sir or Madam]{opening}
\DeclareStringOption[With best regards]{closing}

\ProcessKeyvalOptions{jobacovl}

%% >> vita options ---------------------------------------------------------  %%
\SetupKeyvalOptions{family=jobavita, prefix=jobavita@}

\DeclareStringOption[C.V.]{vitatitle}
\DeclareStringOption[\fmmfamily]{vitacursivefont}
\DeclareStringOption[.5]{vitacolumnratio}
\DeclareStringOption[0.05\textwidth]{vitacolumnsep}
\DeclareStringOption[\faCircle]{vitaskillsymbol}
\DeclareStringOption[\faCircleO]{vitaskillsymbolO}
\DeclareStringOption[\faCircleD]{vitaskillsymbolD}
\DeclareBoolOption{vitaphoto}
\DeclareStringOption{vitaphotopath}

\ProcessKeyvalOptions{jobavita}

%% >> catch option error ---------------------------------------------------  %%
\SetupKeyvalOptions{family=momojoba, prefix=momojoba@}
\DeclareDefaultOption{\ClassWarning{momojoba}{Unknown option '\CurrentOption'}}

\ProcessKeyvalOptions{momojoba}         % process options

%% > Packages =============================================================== %%
%\PassOptionsToClass{10pt, a4paper}{scrartcl}
%\PassOptionsToPackage{twoside=true, twocolumn=true, headinclude=false, footinclude=false}{typearea}
\LoadClass[a4paper]{scrartcl}
\PassOptionsToPackage{bookmarks=false}{hyperref}
\RequirePackage{momokit}
\hypersetup{colorlinks, breaklinks, urlcolor=jobalink, linkcolor=jobalink, filecolor=jobalink}
\RequirePackage{enumitem}

\RequirePackage[firstfoot=false,]{scrletter} %KOMA-script letters
\RequirePackage{paracol} % multi-column layouts that can span pages automatically

\RequirePackage{fontawesome}
\RequirePackage[scale=1]{miama}
\RequirePackage{kmath,kerkis} %The order of the packages matters; kmath changes the default text font
\RequirePackage[sf,scale=0.95]{libertine}
\RequirePackage[letterspace=0]{microtype}

%% > Definitions ============================================================ %%
\NewDocumentCommand\jobasetup{O{}m}{%
    \kvsetkeys{joba#1}{#2}
}
\NewDocumentCommand{\faCircleD}{}{\reflectbox{\faAdjust}}

\newlength{\contactsymbolwidth}
\newlength{\contactinfowidth}
\newlength{\fullcontactwidth}

\newlength{\skillratewidth}
\newlength{\skillinfowidth}
\newlength{\fullskillwidth}

\urlstyle{same}

%% >> vita and covl --------------------------------------------------------  %%
\ExplSyntaxOn
\AtEndPreamble{
    \str_case:onF{\joba@language}{
        {}{}
        {english}{
            \RequirePackage[english]{babel}
        }
        {german}{
            \RequirePackage[ngerman]{babel}
        }
    }{
        \ClassWarning{momojoba}{'\joba@language'~is~not~a~valid~value~language~and~is~being~ignored}
        \RequirePackage[english]{babel}
    }
}

\AtBeginDocument{
    \hypersetup{%
        pdftitle    = \jobacovl@covltitle,
        pdfsubject  = \jobacovl@covlsubject,
        pdfauthor   = \joba@author,
        pdfcreator  = {},
%        pdfproducer = {LaTeX with hyperref},
%        pdfkeywords = {Stichwort1, Stichwort2 ...} ,
    }
    \definecolor{jobatext}{HTML}{\jobacolor@textcolor}%
    \definecolor{jobahighlight}{HTML}{\jobacolor@highlightcolor}%
    \definecolor{jobashade}{HTML}{\jobacolor@shadecolor}%
    \definecolor{jobalink}{HTML}{\jobacolor@linkcolor}%
    \definecolor{jobaskill}{HTML}{\jobacolor@skillcolor}%
%    \str_if_empty:NTF\jobabasetype{}
    \str_case:onF{\joba@type}{
            {}{}
            {covl}{
                \ClassWarning{momojoba}{covl}
                \begin{covl}
                \AtEndDocument{\end{covl}}
            }
            {vita}{
                \ClassWarning{momojoba}{vita}
                \begin{vita}
                \AtEndDocument{\end{vita}}
            }
        }{\ClassWarning{momojoba}{'\joba@type'~is~not~a~valid~value~in~\ jobasetup~and~is~being~ignored}}
}
\ExplSyntaxOff

%% >> cover letter ---------------------------------------------------------  %%
\NewDocumentEnvironment{covl}{}{%
%    \@setplength{firstheadwidth}{\textwidth}
%    \@setplength{toaddrhpos}{\oddsidemargin}
%    \@addtoplength{toaddrhpos}{1in}
    \setkomavar{firsthead}{%
        \setlength{\contactsymbolwidth}{13pt}
        \setlength{\contactinfowidth}{(\linewidth-\jobavita@vitacolumnsep)*\real{\jobavita@vitacolumnratio}}
        \settowidth{\fullcontactwidth}{%
            \colorbox{black}{%
                \tabular{C{\contactsymbolwidth}|L{\contactinfowidth}N}&&\endtabular%
            }%
        }
        \setlength{\contactinfowidth}{\linewidth-\jobavita@vitacolumnsep-\fullcontactwidth}
        \begin{minipage}{\textwidth}%
            \hspace{\fill}
            \colorbox{jobashade}{%
                \tabular{C{\contactsymbolwidth}|L{\contactinfowidth}N}
                \faUser & \joba@author\\
                \faHome & \expandafter\ifblank\expandafter{\jobacontact@fulladdress}{%
                    \jobacontact@street~\jobacontact@streetnr, \jobacontact@postalcode~\jobacontact@city%
                }{\jobacontact@fulladdress}&\\%Address
                \faPhone &\jobacontact@phone&\\%Phone number
                \expandafter\notblank\expandafter{\jobacontact@email}{\small\faAt & \href{mailto:\jobacontact@email}{\jobacontact@email}\textcolor{jobashade}{;}&\\}{}%Email address
                \expandafter\notblank\expandafter{\jobacontact@homepage}{%homepage
                    \expandafter\notblank\expandafter{\jobacontact@homepageshort}{
                        \faGlobe & \href{\jobacontact@homepage}{\nolinkurl{\jobacontact@homepageshort}}&\\
                    }{
                        \faGlobe & \url{\jobacontact@homepage}&\\
                    }
                }{}
                \endtabular%
            }
        \end{minipage}%
    }%
    \addtokomafont{lettertitle}{\raggedright\color{jobahighlight}}
    \expandafter\ifblank\expandafter{\jobacovl@covltitle}{}{\setkomavar{title}{\jobacovl@covltitle}}
    \addtokomafont{lettersubject}{\color{jobahighlight}\Large\sffamily}
    \expandafter\ifblank\expandafter{\jobacovl@covlsubject}{}{\setkomavar{subject}{\jobacovl@covlsubject}}
    \setkomavar{date}{\jobacovl@covldate}
    \setkomavar{fromname}{\joba@author}
    \setkomavar{fromaddress}{\jobacontact@street\ \jobacontact@streetnr\\ \jobacontact@postalcode\ \jobacontact@city}
    \setkomavar{fromphone}{\jobacontact@phone}
    \setkomavar{fromemail}{\jobacontact@email}
%    \setkomavar{fromfax}{+31 (0)71 5144543}
%    \setkomavar{fromurl}{http://www.kindoblue.nl}
%    \setkomavar{frombank}{Postbank 9307157}
%    \setkomavar{place}{Amsterdam}
    \setkomavar{signature}{\joba@author}
    \renewcommand*{\raggedsignature}{\raggedright}
    \begin{letter}[foldmarks=false, backaddress=false, pagenumber=false]{\textbf{\jobacovl@recipientcompany}\\\jobacovl@recipientperson\\\jobacovl@recipientstreet\ \jobacovl@recipientstreetnr\\\jobacovl@recipientpostalcode\ \jobacovl@recipientcity}
    \opening{\jobacovl@opening}
}{\closing{\jobacovl@closing}\end{letter}}

%% >> curriculum vitae -----------------------------------------------------  %%
\NewDocumentEnvironment{vita}{O{\joba@title}}{%
%% >>> new environments
    \NewDocumentCommand{\vitasectionrule}{}{\vspace{-1.5em}\rule{\linewidth}{0.8pt}}
%% >>> latest project/job
    \NewDocumentEnvironment{recentproject}{O{Recent Project}m}{
        \section{##1}\vitasectionrule\\
        \textbf{##2}
        \smallskip\\
    }{}
%% >>> work experiece
    \NewDocumentEnvironment{workexp}{O{Work Experience}}{
        \NewDocumentCommand{\workposition}{mmO{}O{}}{
            \raggedleft{\small\textsc{####1}}\\
            \raggedright\textbf{\large####2}\\
            \textit{####3}\\
            \notblank{####4}{\smallskip####4\\\medskip}{\medskip}
        }
        \section{##1}\vitasectionrule
    }{}
%% >>> education
    \NewDocumentEnvironment{education}{O{Education}O{align=center}}{%
        \NewDocumentCommand{\educationentry}{mmO{}O{}O{}}{%
            \item[\small\textsc{####1}]\textbf{\large####2}\hspace{\fill}{\small\textsc{####5}}\\%
            \textit{####3}\\
            ####4
        }%
        \section{##1}\vitasectionrule%
        \newlength{\eduyear}%
        \setlength{\eduyear}{\widthof{\small\textsc{8888 -- 888}}}%
        \SetLabelAlign{center}{\makebox[\eduyear]{####1}}%
        \begin{description}[labelwidth=\eduyear, itemsep=1ex, labelsep=1em, leftmargin=!,font=\normalfont, ##2]
        }{%
        \end{description}
    }
%% >>> skills
    \NewDocumentEnvironment{skills}{O{Skills}}{%
        \NewDocumentCommand{\skill}{mm}{
            \ifcase####1\or
            \item[\color{jobaskill}\footnotesize\jobavita@vitaskillsymbolD\ \jobavita@vitaskillsymbolO\ \jobavita@vitaskillsymbolO] ####2\medskip
            \or
            \item[\color{jobaskill}\footnotesize\jobavita@vitaskillsymbol\ \jobavita@vitaskillsymbolO\ \jobavita@vitaskillsymbolO] ####2\medskip
            \or
            \item[\color{jobaskill}\footnotesize\jobavita@vitaskillsymbol\ \jobavita@vitaskillsymbolD\ \jobavita@vitaskillsymbolO] ####2\medskip
            \or
            \item[\color{jobaskill}\footnotesize\jobavita@vitaskillsymbol\ \jobavita@vitaskillsymbol\ \jobavita@vitaskillsymbolO] ####2\medskip
            \or
            \item[\color{jobaskill}\footnotesize\jobavita@vitaskillsymbol\ \jobavita@vitaskillsymbol\ \jobavita@vitaskillsymbolD] ####2\medskip
            \or
            \item[\color{jobaskill}\footnotesize\jobavita@vitaskillsymbol\ \jobavita@vitaskillsymbol\ \jobavita@vitaskillsymbol] ####2\medskip
            \fi
        }
        \section{##1}\vitasectionrule
        \begin{itemize}[align=left, labelsep=1em, labelwidth=\widthof{\footnotesize\jobavita@vitaskillsymbol\ \jobavita@vitaskillsymbol\ \jobavita@vitaskillsymbol}, leftmargin=!]
        }{
        \end{itemize}
    }
    \NewDocumentEnvironment{plainskills}{O{Skills}}{
        \NewDocumentCommand{\plainskill}{mO{}}{
            \notblank{####2}{
                \textit{####1} -- ####2\medskip\\
            }{
                \textit{####1}\medskip\\
            }
        }
        \section{##1}\vitasectionrule
        \raggedright
    }{}
    \NewDocumentEnvironment{languages}{O{Languages}}{
        \NewDocumentCommand{\vitalanguage}{mO{}O{}}{
            \textbf{####1}\hspace{\fill}{####2}\\
            ####3\medskip\\
        }
        \section{##1}\vitasectionrule
        \raggedright
    }{}
%% >>> publications
\NewDocumentEnvironment{publications}{O{Publications}}{
	\NewDocumentCommand{\paper}{mmO{}O{}O{}}{
		####1%
		
		\vspace{0.5mm}\quad ####3\notblank{####4}{ ---~\textit{####4}\notblank{####5}{####5}{}
		}{} (####2)
		
		\bigskip
	}
	\section{##1}\vitasectionrule
}{}
%
    \columnratio{\jobavita@vitacolumnratio,1-\jobavita@vitacolumnratio} % The relative ratios of the two columns in the CV
    \setlength\columnsep{\jobavita@vitacolumnsep} % Specify the amount of space between the columns
    \setlength{\parindent}{0pt}        % setzt den Absatzeinzug
    \setlist{nosep}
%
    \settowidth{\skillratewidth}{\footnotesize\jobavita@vitaskillsymbol\ \jobavita@vitaskillsymbol\ \jobavita@vitaskillsymbol}
    \setlength{\skillinfowidth}{(\linewidth-\columnsep)*\real{\jobavita@vitacolumnratio}}
    \settowidth{\fullskillwidth}{%
        \tabular{m{\skillratewidth}p{\skillinfowidth}N}&&
        \endtabular%
    }
    \setlength{\skillinfowidth}{\linewidth-\columnsep-\fullskillwidth}
    \setlength{\contactsymbolwidth}{13pt}
    \setlength{\contactinfowidth}{(\linewidth-\jobavita@vitacolumnsep)*\real{\jobavita@vitacolumnratio}}
    \settowidth{\fullcontactwidth}{%
        \colorbox{black}{%
            \tabular{C{\contactsymbolwidth}|L{\contactinfowidth}N}&&\endtabular%
        }%
    }
    \setlength{\contactinfowidth}{\linewidth-\jobavita@vitacolumnsep-\fullcontactwidth}
    \setkomafont{sectioning}{\color{jobahighlight}\scshape}
    \setkomafont{section}{\LARGE}
    \renewcommand*{\sectionformat}{}
    \color{jobatext}\thispagestyle{empty}
%% >>> vita contact box
    \newsavebox{\vitacontactbox}
    \savebox{\vitacontactbox}{\colorbox{jobashade}{%
            \begin{tabular}{C{\contactsymbolwidth}|L{\contactinfowidth}N}
                \faHome & \expandafter\ifblank\expandafter{\jobacontact@fulladdress}{%
                    \jobacontact@street~\jobacontact@streetnr, \jobacontact@postalcode~\jobacontact@city%
                }{\jobacontact@fulladdress}&\\%Address
                \faPhone &\jobacontact@phone&\\%Phone number
                \expandafter\notblank\expandafter{\jobacontact@email}{\small\faAt & \href{mailto:\jobacontact@email}{\jobacontact@email}\textcolor{jobashade}{;}&\\}{}%Email address
                \expandafter\notblank\expandafter{\jobacontact@homepage}{%homepage
                    \expandafter\notblank\expandafter{\jobacontact@homepageshort}{
                        \faGlobe & \href{\jobacontact@homepage}{\nolinkurl{\jobacontact@homepageshort}}&\\
                    }{
                        \faGlobe & \url{\jobacontact@homepage}&\\
                    }
                }{}
                \expandafter\notblank\expandafter{\jobacontact@linkedin}{%linkedin
                    \expandafter\notblank\expandafter{\jobacontact@linkedinshort}{
                        \faLinkedin & \href{\jobacontact@linkedin}{\nolinkurl{\jobacontact@linkedinshort}}&\\
                    }{
                        \faLinkedin & \url{\jobacontact@linkedin}&\\
                    }
                }{}
                \expandafter\notblank\expandafter{\jobacontact@xing}{%xing
                    \expandafter\notblank\expandafter{\jobacontact@xingshort}{
                        \faXing & \href{\jobacontact@xing}{\nolinkurl{\jobacontact@xingshort}}&\\
                    }{
                        \faXing & \url{\jobacontact@xing}&\\
                    }
                }{}
    \end{tabular}}}
%% >>> vita head
    \ifjobavita@vitaphoto
        \makevitaphotohead
    \else
        \makevitahead
    \fi
    \columnratio{\jobavita@vitacolumnratio}
    \begin{paracol}{2}%
}{\end{paracol}}

\NewDocumentCommand{\makevitahead}{}{%
    \begin{paracol}{2}
        \begin{minipage}[center][0.12\textheight][c]{\linewidth}
            \centering
            \sffamily\Huge\joba@author\\\medskip
            \color{jobahighlight}
            \LARGE\jobavita@vitacursivefont{\jobavita@vitatitle}
        \end{minipage}
        \switchcolumn
        \begin{minipage}[center][0.12\textheight][c]{\linewidth}%
            \usebox{\vitacontactbox}
        \end{minipage}
    \end{paracol}
    \vspace{0.01\textheight}
}

\NewDocumentCommand{\makevitaphotohead}{}{%
    \newlength{\vcboxh}
    \newlength{\vcboxd}
    \setlength{\vcboxh}{\heightof{\usebox{\vitacontactbox}}}
    \setlength{\vcboxd}{\depthof{\usebox{\vitacontactbox}}}
    \begin{paracol}{2}
        \begin{minipage}[center][0.1\textheight][c]{\linewidth}
            \begin{minipage}[center][0.1\textheight][c]{.75\vcboxh+.75\vcboxd}
                \expandafter\ifblank\expandafter{\jobavita@vitaphotopath}{%
                    \rule[0pt]{2cm}{\vcboxh+\vcboxd}%
                }{%
                    \includegraphics[height=\vcboxh+\vcboxd,]{\jobavita@vitaphotopath}%
                }
            \end{minipage}
            \begin{minipage}[center][0.1\textheight][c]{\linewidth-.75\vcboxh-.75\vcboxd}%
                \centering
                \sffamily\huge\joba@author\medskip\\
                \color{jobahighlight}
                \Large\jobavita@vitacursivefont{\jobavita@vitatitle}
            \end{minipage}
        \end{minipage}
        \switchcolumn[1]
        \begin{minipage}[center][0.1\textheight][c]{\linewidth}%
            \usebox{\vitacontactbox}
        \end{minipage}
    \end{paracol}
    \vspace{0.01\textheight}
}
\endinput
%% ========================================================================== %%
%%  Changelog
%%  ==========
%%  Unreleased
%%  ----------
%%  - 2019-07-05: adapted to 80 columns per line limit
%%  
%%  Version 2.3 - 2018-09-19
%%  ----------
%%  - use of savebox for contactinfo
%%  - alignment option for education
%%  - options for opening, closing of cover letter
%%  - dirty fix of the mailto: bug
%%  
%%  Version 2.2 - 2018-08-02
%%  ----------
%%  - added possibility for a photo in the vita
%%  - changed appearance of work positions
%%  - fixed a lot of spacing
%%  
%%  Version 2.1
%%  ----------
%%  - 2018-06-14: included change to momokit & momobib
%%  - added cover letter functionality
%% ========================================================================== %%