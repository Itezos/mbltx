%% ========================================================================== %%
%%  momorept.cls v3.0 - class for reports, theses etc.                        %%
%%  Copyright (c) 2021 Moritz Burmeister                                      %%
%%                                                                            %%
%%  This work may be distributed and/or modified under the                    %%
%%  conditions of the LaTeX Project Public License, either version 1.3        %%
%%  of this license or (at your option) any later version.                    %%
%%  The latest version of this license is in                                  %%
%%    http://www.latex-project.org/lppl.txt                                   %%
%%                                                                            %%
%%  This work has the LPPL maintenance status 'maintained'.                   %%
%%  The Current Maintainer of this work is Moritz Burmeister.                 %%
%%  ------------------------------------------------------------------------  %%
%%  This work is part of the 'mbltx' bundle, which can be found at:           %%
%%    https://gitlab.com/Itezos/mbltx                                         %%
%% ========================================================================== %%
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{momorept}[2021-03-21 v3.0 class for reports, theses etc.]

%% > Options ================================================================ %%
\RequirePackage{kvoptions}              % define key=value options
\RequirePackage{etoolbox}
%% >>> option for short/long documents
\DeclareBoolOption{lang}
\DeclareComplementaryOption{kurz}{lang}
%% >>> option for double/single sided documents
\DeclareBoolOption{doppel}
\DeclareComplementaryOption{einzel}{doppel}
%% >>> option for english/german documents
\DeclareBoolOption{deutsch}
\DeclareComplementaryOption{englisch}{deutsch}
%% >>> option for references
\DeclareStringOption[]{quellen}[references.bib]
%% >>> catch option error
\DeclareDefaultOption{\ClassWarning{momorept}{Unknown option '\CurrentOption'}}
\ProcessKeyvalOptions{momorept}         % process options

%% > Class setup ============================================================ %%
\ifmomorept@lang
    \LoadClassWithOptions{scrreprt}     % builds on KOMA report class
\else
    \LoadClassWithOptions{scrartcl}     % builds on KOMA article class
\fi
%\KOMAoptions{DIV=calc}

%% > Packages =============================================================== %%
%\RequirePackage[]{scrhack}             % KOMA-compatibility
\RequirePackage[T1]{fontenc}            % output characters like ä,ö,ü
\RequirePackage[utf8]{inputenc}         % input characters like ä,ö,ü
\RequirePackage{lmodern}                % vector based font
%% >>> base functionality for mbltx bundle
\AtEndPreamble{\RequirePackage{momokit}}

%% >> Languages ------------------------------------------------------------  %%
\ifmomorept@deutsch                     
    \RequirePackage[english,ngerman]{babel}
\else
     \RequirePackage[ngerman,english]{babel}
%% >>> change title of ToC from 'Contents' to 'Table of Contents'
     \addto{\captionsenglish}{%
          \renewcommand*{\contentsname}{Table of Contents}
      }
\fi
%\RequirePackage{csquotes}   % quotation marks

%% >> Bibliography ---------------------------------------------------------  %%
%% >>> *.bib file is taken from `quellen=` option
%% >>> \defbibheading{bibliography}[\refname]{}
%% >>> no title -> use chapter/section
%% >>> or use \printbibliography[heading=none]
\ifdefempty{\momorept@quellen}{}{
    \RequirePackage[quellen = \momorept@quellen]{momobib}
}
%% >> Header & Footer ------------------------------------------------------  %%
\RequirePackage[draft=false]{scrlayer-scrpage}

%% > Formatting ============================================================= %%
%% >> Header & Footer ------------------------------------------------------  %%
\clearscrheadfoot
\setkomafont{pagehead}{\textsc}         % font = small caps

\ifmomorept@doppel
%% >>> text is set two sided
%% >>> left head on even pages shows section/chapter
%% >>> right head on odd pages shows subsection/section
%% >>> page number is put in the outer lower corners of each page
    \KOMAoptions{twoside=true}
    \lehead{\leftmark}
    \rohead{\rightmark}
    \ofoot[\pagemark]{\pagemark}
\else
%% >>> text is set one sided
%% >>> subsection/section is put in the right head
%% >>> page number is put in the right lower corner of each page
    \KOMAoptions{twoside=false}
    \rohead{\leftmark}
    \rofoot[\pagemark]{\pagemark}
\fi

%% >> Numeration -----------------------------------------------------------  %%
\ifmomorept@lang
     \automark[section]{chapter}
% \@addtoreset{figure}{chapter}
% \renewcommand{\thefigure}{\thechapter.\arabic{figure}}
\else
     \automark[subsection]{section}
\fi

%% >> Acronyms -------------------------------------------------------------  %%
\AfterPackage{acro}{
    \DeclareAcroListStyle{tabelle}{table}{table-spec=rp{.7\linewidth}}
    \acsetup{
        list-short-format={\small\bfseries\sffamily},
        list-long-format={\small\sffamily},
        list-style=tabelle,
        hyperref=true,macros=false,only-used=false
    }
}

%% >> Captions -------------------------------------------------------------  %%
\AfterPackage{caption}{
    \captionsetup{
        format=plain,
        singlelinecheck=1,
        margin*=\parindent,
    }
}

%% >> Chemistry ------------------------------------------------------------  %%
\AfterPackage{chemmacros}{
    \chemsetup{
        formula=chemformula,
        greek=textgreek,
        modules={polymers,spectroscopy},
    }
    \chemsetup[spectroscopy]{
        delta=(ppm),
        use-equal=true,
    }
}

\AfterPackage{chemfig}{
    \renewcommand*\printatom[1]{\ensuremath{\mathsf{#1}}} % sans serif
    \setangleincrement{30} % 30° angle increment between bonds in chemfig
}

%% >> SI units -------------------------------------------------------------  %%
\AfterPackage{siunitx}{
    \sisetup{detect-all,
        per-mode=symbol,
        range-units=single,
        list-units=single,
        product-units=single,
    }
}

%% >> Page breaks ----------------------------------------------------------  %%
\clubpenalty = 10000 % Schusterjungen
\widowpenalty = 10000 % Hurenkinder

%% >> Paragraphs -----------------------------------------------------------  %%
%\setlength{\parskip}{0em}   % setzt den Absatzabstand
%\setlength{\parindent}{2em}  % setzt den Absatzeinzug
%\captionsetup{margin*=\parindent} % muss hier nochmal gesetzt werden

%% >> Greek symbols --------------------------------------------------------  %%
\renewcommand{\pi}{\textrm{\textpi}}   % schönes aufrechtes Pi
\renewcommand{\epsilon}{\ensuremath\varepsilon} % geschwungenes epsilon
\renewcommand{\theta}{\ensuremath\vartheta}  % geschwungenes Theta
\renewcommand{\phi}{\ensuremath\varphi}   % geschwungenes Phi

\endinput
%% ========================================================================== %%
%%  Changelog
%%  ==========
%%  Unreleased
%%  ----------
%%  - 2019-07-06: moved bibliography formating to momobib
%%  - 2019-07-05: adapted to 80 columns per line limit
%%  - 2018-06-25: changed DefaultOption / error message
%%  
%%  Version 2.4
%%  ----------
%%  - 2018-06-15: added `acro` formatting
%%  - 2018-06-14: included change to momokit & momobib
%%  - removed scrhack (apparent from log file it is not needed)
%%  
%%  Version 2.3
%%  ----------
%%  - set angle increment for chemfig to 30°
%%  - moved options from `biblatex` to \ExecuteBibliographyOptions
%%  - changed order so that `momo` is loaded after `babel`
%%  - fixed typo in siuintx
%%  
%%  Version 2.2
%%  ----------
%%  - moved all commands to momo.sty
%%  - cleaned up
%%  - `biblatex` is loaded conditionally
%%  
%%  Version 2.1
%%  ----------
%%  - revised captions
%%  - rearanged "Other Commands"
%%  
%%  Version 2.0
%%  ----------
%%  - started changelogging
%%  - `DIV=calc` added
%% ========================================================================== %%