%% ========================================================================== %%
%%  momopres.cls v3.0 - class for presentations                               %%
%%  Copyright (c) 2021 Moritz Burmeister                                      %%
%%                                                                            %%
%%  This work may be distributed and/or modified under the                    %%
%%  conditions of the LaTeX Project Public License, either version 1.3        %%
%%  of this license or (at your option) any later version.                    %%
%%  The latest version of this license is in                                  %%
%%    http://www.latex-project.org/lppl.txt                                   %%
%%                                                                            %%
%%  This work has the LPPL maintenance status 'maintained'.                   %%
%%  The Current Maintainer of this work is Moritz Burmeister.                 %%
%%  ------------------------------------------------------------------------  %%
%%  This work is part of the 'mbltx' bundle, which can be found at:           %%
%%    https://gitlab.com/Itezos/mbltx                                         %%
%% ========================================================================== %%
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{momopres}[2021-03-21 v3.0 class for presentations]
\PassOptionsToClass{aspectratio=1610,compress}{beamer}
\PassOptionsToPackage{dvipsnames,x11names}{xcolor}
\LoadClass{beamer}
%% > Options ================================================================ %%
\DeclareStringOption[black]{farbe}[black]
\DeclareBoolOption{deutsch}
\DeclareComplementaryOption{englisch}{deutsch}
\DeclareStringOption[]{quellen}[references.bib]
\DeclareBoolOption{grid}
\ProcessKeyvalOptions{momopres}

%% > Packages =============================================================== %%
\RequirePackage[utf8]{inputenc}		% Zeichen wie ä,ö,ü eingeben
\RequirePackage[T1]{fontenc}		% Zeichen wie ä,ö,ü ausgeben
\RequirePackage{lmodern}			% Vektor basierte Schriftart
%% >>> base functionality for mbltx bundle
\AtEndPreamble{\RequirePackage{momokit}}

%% >> Languages ------------------------------------------------------------  %%
\ifmomopres@deutsch
	\RequirePackage[english,ngerman]{babel}	% deutsch als Hauptsprache
\else
	\RequirePackage[ngerman,english]{babel}	% english als Hauptsprache
	\addto{\captionsenglish}{\renewcommand*{\contentsname}{Outline}}
% ändert den Titel des TOC von "Contents" zu "Outline"
\fi
\RequirePackage{csquotes}			% quotation marks
%% >> Bibliography ---------------------------------------------------------  %%
%% >>> *.bib file is taken from `quellen=` option
%% >>> \defbibheading{bibliography}[\refname]{}
%% >>> no title -> use chapter/section
%% >>> or use \printbibliography[heading=none]
\ifdefempty{\momopres@quellen}{}{
	\RequirePackage[
		backend=biber,
		style=nature,
			]{biblatex}
	\ExecuteBibliographyOptions{
		sorting=ynt,
		maxnames=2,
			}
	\DeclareNameAlias{author}{first-last}
	\renewcommand*{\bibfont}{\footnotesize}
	\addbibresource{\momopres@quellen}
	\RequirePackage{momobib}
}
% .bib file wird von option quellen= übernommen
% \defbibheading{bibliography}[\refname]{}
% ohne Titel -> chapter/section benutzen
% einfacher \printbibliography[heading=none]
%% >> Positioning ----------------------------------------------------------  %%
\RequirePackage[
	absolute,
	overlay,
		]{textpos}
		
%% > Formatting ============================================================= %%
%% >> Grid -----------------------------------------------------------------  %%
\ifmomopres@grid				% hilft beim Positionieren von Bildern/Text
	\setbeamertemplate{background}[grid]
\else\fi
%% >> General Layout -------------------------------------------------------  %%
\useoutertheme[			% circles on the top of the slide
	subsection=false,	% to jump between slides
		]{miniframes}
\useinnertheme{default}
\usefonttheme[]{structurebold}
\usecolortheme{seagull}
%% >> Color Layout ---------------------------------------------------------  %%
\colorlet{farbe}{\momopres@farbe}
\setbeamercolor*{palette tertiary}{fg=unigrau!100,bg=unigrau!10}
\setbeamercolor{normal text}{fg=black,bg=white}
\setbeamercolor{alerted text}{fg=farbe}
\setbeamercolor{title}{fg=white,bg=farbe!95}
\setbeamercolor{frametitle}{fg=white,bg=farbe!95}
\setbeamercolor{framesubtitle}{fg=white,bg=farbe!75}
\setbeamercolor{footline}{fg=unigrau!100,bg=unigrau!10}
\setbeamercolor{block title}{fg=white,bg=farbe!85}
\setbeamercolor{block body}{fg=black,bg=farbe!15}
\setbeamercolor{itemize item}{fg=farbe}
\setbeamercolor{eunmerate item}{bg=farbe}
\renewcommand*{\bibfont}{\footnotesize}
\setbeamertemplate{bibliography item}[triangle]
\setbeamercolor{bibliography item}{fg=farbe!80}
%% >> frame title & subtitle -----------------------------------------------  %%
\newlength{\fratiwi}	% frame title width
\setlength{\fratiwi}{\paperwidth-\leftmargini}
\setbeamertemplate{frametitle}{
	\begin{textblock*}{\fratiwi}[0,0](\leftmargini,0.7cm)
		\begin{beamercolorbox}[dp=1.25ex,ht=2.5ex,leftskip=.7ex]{frametitle}
			\insertframetitle
		\end{beamercolorbox}
	\end{textblock*}\vspace*{1cm}
}
\newcommand{\untertitel}[1]{
	\begin{textblock*}{\fratiwi}[0,0](\leftmargini,2.25\leftmargini)
		\begin{beamercolorbox}[dp=1ex,ht=2.25ex,leftskip=.7ex]{framesubtitle}
			\hspace{.5ex}\bfseries{#1}
		\end{beamercolorbox}
	\end{textblock*}\vspace*{1\leftmargini}
}
%% >> Naviagtion Symbols & Footer ------------------------------------------  %%
\setbeamertemplate{navigation symbols}{}
\newlength{\platz}
\setlength{\platz}{0.1cm}
\newlength{\fussplatz}
\setlength{\fussplatz}{\paperwidth-2\platz}
\setbeamertemplate{footline}{
	\begin{beamercolorbox}[sep=\platz]{footline}
%		\hspace*{1em}
		\centering
		\parbox[t]{0.333\fussplatz-1em}{\raggedright\inserttitle}
		\parbox[t]{0.333\fussplatz}{\centering\insertframenumber}
		\parbox[t]{0.333\fussplatz-1em}{\raggedleft\insertauthor}
%		\hspace*{1em}
	\end{beamercolorbox}
}
%% >> Chemistry ------------------------------------------------------------  %%
\AfterPackage{chemmacros}{
	\chemsetup{
		formula=chemformula,
		greek=textgreek,
		modules={polymers,spectroscopy},
	}
	\chemsetup[spectroscopy]{
		delta=(ppm),
		use-equal=true,
	}
}
\AfterPackage{chemfig}{
	\renewcommand*\printatom[1]{\ensuremath{\mathsf{#1}}}	% sans serif
	\setangleincrement{30}	% 30° angle increment between bonds in chemfig
}
%% >> links inside the document --------------------------------------------  %%
\AfterPackage{hyperref}{
	\hypersetup{
		%	bookmarks=true,
		colorlinks=true,
		citecolor=black,
		filecolor=black,
		linkcolor=black,
		urlcolor=black,
	}
}
%% >> SI units -------------------------------------------------------------  %%
\AfterPackage{siuintx}{
	\sisetup{detect-all,
		per-mode=symbol,
		range-units=single,
		list-units=single,
		product-units=single,
	}
}
%% >> Greek symbols --------------------------------------------------------  %%
\renewcommand{\pi}{\textrm{\textpi}}			% schönes aufrechtes Pi
\renewcommand{\epsilon}{\ensuremath\varepsilon}	% geschwungenes epsilon
\renewcommand{\theta}{\ensuremath\vartheta}		% geschwungenes Theta
\renewcommand{\phi}{\ensuremath\varphi}			% geschwungenes Phi

%	-	Error
\DeclareDefaultOption{\@unknownoptionerror}
\endinput
%% ========================================================================== %%
%%  Changelog
%%  ==========
%	-	Version 2.1
%	passed dvipsnames to xcolor to make more colors available
%	set angle increment for chemfig to 30°
%	moved options from biblatex to \ExecuteBibliographyOptions
%	changed order so that momo is loaded after babel
%	-	Version 2.0
%	started changelogging
%	moved all commands to momo.sty
%	cleaned up
%% ========================================================================== %%