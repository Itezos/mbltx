mbltx
===
`21. March 2021`

>Author: Moritz Burmeister

A collection of classes and packages to make LaTeX life easier.

Installation
---
Depending on your LaTeX distribution your local texmf tree might differ. So the easiest way is to put the bundle files right in the directory of the TeX-file your working on.

Usage
---

This is just a quick start. For a more detailed introduction take a look at the documentation.

### momokit

Like any other package `momokit` is employed with:

```latex
\usepackage[]{momokit}
```

> chemie = true|<u>false</u>

This package has one Boolean option called `chemie` that toggles the inclusion of two huge packages related to chemistry (`chemmacros`, `chemfig`).

### momorept

To format your document in momostyle:

```latex
\documentclass[]{momorept}
```

> kurz = <u>true</u>|false **or** lang = true|<u>false</u>

Choose between formatting for different lengths of documents.  By default (`kurz`) the highest sectioning level is `\section{}`. The options are complementary, you only need one.

> einzel = <u>true</u>|false| **or** doppel = true|<u>false</u>

Choose between one or two-sided layout. The options are complementary, you only need one.

> englisch= <u>true</u>|false| **or** deutsch= true|<u>false</u>

Determines which of `ngerman` and `english` is the main language of the `babel`package. Again complementary options.

### momobib

```latex
\usepackage{momobib}
```

> quellen = `<bibfile.bib>`

The bibliography resource is added from `<bibfile.bib>` . The default is `references.bib`.

### momojoba

```latex
\documentclass[]{momojoba}
```

Use `\jobasetup{}` to pass your information to the class.

### momopres

*No longer in active development.*

ToDo
---

- [x] write README.md
- [ ] add detailed documentation with `cnltx-doc`
- [x] fix momobib problem wioth double lastnames


