%% ========================================================================== %%
%%  momokit.sty v3.0 - useful commands for LaTeX documents                    %%
%%  Copyright (c) 2021 Moritz Burmeister                                      %%
%%                                                                            %%
%%  This work may be distributed and/or modified under the                    %%
%%  conditions of the LaTeX Project Public License, either version 1.3        %%
%%  of this license or (at your option) any later version.                    %%
%%  The latest version of this license is in                                  %%
%%    http://www.latex-project.org/lppl.txt                                   %%
%%                                                                            %%
%%  This work has the LPPL maintenance status 'maintained'.                   %%
%%  The Current Maintainer of this work is Moritz Burmeister.                 %%
%%  ------------------------------------------------------------------------  %%
%%  This work is part of the 'mbltx' bundle, which can be found at:           %%
%%    https://gitlab.com/Itezos/mbltx                                         %%
%% ========================================================================== %%
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{momokit}[2021-03-21 v3.0 useful commands for LaTeX documents]

%% > Options ================================================================ %%
\RequirePackage{kvoptions}              % key=value optionen definieren
\RequirePackage{etoolbox}
\DeclareBoolOption[false]{chemie}
\ProcessKeyvalOptions{momokit}

%% > Packages =============================================================== %%
%\RequirePackage[]{scrhack}             % KOMA-compatibility
\RequirePackage[T1]{fontenc}            % output characters like ä,ö,ü
\RequirePackage[utf8]{inputenc}         % input characters like ä,ö,ü
\RequirePackage{acro}                   % package for acronyms
\RequirePackage{amsmath,amssymb}        % math commands and symbols
\RequirePackage{array}                  % commands for tables
\RequirePackage{blindtext}              % testing
\RequirePackage{calc}                   % length calculations
\RequirePackage{caption}                % commands for captions
\RequirePackage{csquotes}               % quotation marks
\RequirePackage[e]{esvect}              % scaleable vector arrows
\RequirePackage{graphicx}               % for \includegraphics{}
\RequirePackage{siunitx}                % typeset units
\RequirePackage[list=true]{subcaption}  % divided figures
\RequirePackage{textgreek}              % \text<greek letter>
\RequirePackage{wrapfig}                % wrapped things
\RequirePackage{xcolor}                 % colors

%% >> Chemistry ------------------------------------------------------------  %%
\ifmomokit@chemie                       % if chemie=true
 \RequirePackage{chemmacros}            % if needed because huge package
 \RequirePackage{chemfig}
\else\fi

%% >> links inside the pdf -------------------------------------------------  %%
\AtEndOfPackage{\RequirePackage{hyperref}}

%% > Commands =============================================================== %%
%% >> Abstracts ------------------------------------------------------------  %%
%% >>> a seperate page for abstracts
\newenvironment{abstractpage}
{\clearpage\thispagestyle{empty}}
{}
%% >>> formating of abstracts
\DeclareDocumentEnvironment{abstract}{m}
{\selectlanguage{#1}%
\begin{flushleft}\huge\bfseries\sffamily\abstractname\end{flushleft}}
{}
%\renewenvironment{abstract}[1]  % Formatierung für abstracts
%{\selectlanguage{#1}%
% \begin{flushleft}\chapter*{\abstractname}\end{flushleft}}
%{}

%% >> Captions -------------------------------------------------------------  %%
%% >>> bold caption
\newcommand{\bfcaption}[2][]{%
    \ifstrempty{#1}{\caption{#2}}{\caption[#1]{\textsf{\textbf{#1}} -- #2}}
% \caption[#1]{\textsf{\textbf{#1}}\ifstrempty{#1}{}{ -- }#2}
}
%% >>> small caps caption
\newcommand{\sccaption}[2][]{%
    \caption[#1]{\textsc{#1}\ifstrempty{#1}{}{ -- }#2}
}

%\DeclareCaptionLabelFormat{blank}{}
\newcommand{\sucaption}[1]{
 \captionsetup{textformat=empty}
 \caption{#1}
  }
%\captionsetup{textformat=empty,labelformat=blank}

%% >> Table columns --------------------------------------------------------  %%
%% >>> requires `array` package & provides new vertically centered column
%% >>> types with required widths (L=left, C=center, R=right)
\newcolumntype{L}[1]{>{\raggedright\arraybackslash}m{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\arraybackslash}m{#1}}
%% >>> new column type to determine line height by adding the additional
%% >>> column at tabular start. Remember extra `&` in each line!
\newcolumntype{N}{@{}m{0pt}@{}}

%% > Colors ================================================================= %%
%% >> Uni Tuebingen: primary colors ----------------------------------------  %%
\definecolor{unirot}{HTML}{A51E37}      % = 165.30.50
\definecolor{unigold}{HTML}{B4A069}     % = 180.160.105
\definecolor{unigrau}{HTML}{324148}     % = 50.65.75

%% >> Uni Tuebingen: secondary colors --------------------------------------  %%
\definecolor{uni01}{HTML}{415A8C}       % = 65.90.140
\definecolor{uni02}{HTML}{0069AA}       % = 0.105.170
\definecolor{uni03}{HTML}{50AAC8}       % = 80.170.200
\definecolor{uni04}{HTML}{82B9A0}       % = 130.185.160
\definecolor{uni05}{HTML}{7DA54B}       % = 125.165.75
\definecolor{uni06}{HTML}{326E1E}       % = 50.110.30
\definecolor{uni07}{HTML}{C8503C}       % = 200.80.60
\definecolor{uni08}{HTML}{AF6E96}       % = 175.110.150
\definecolor{uni09}{HTML}{B4A096}       % = 180.160.150
\definecolor{uni10}{HTML}{D7B469}       % = 215.180.105
\definecolor{uni11}{HTML}{D29600}       % = 210.150.0
\definecolor{uni12}{HTML}{916946}       % = 145.105.70

%% >> color palette ----------------------------------------------------------%%
\definecolor{diagram01}{HTML}{004080}   % = 0.64.128
\definecolor{diagram02}{HTML}{FF8000}   % = 255.128.0
\definecolor{diagram03}{HTML}{008040}   % = 0.128.64
\definecolor{diagram04}{HTML}{FF0080}   % = 255.0.128
\definecolor{diagram05}{HTML}{400080}   % = 64.0.128
\definecolor{diagram06}{HTML}{80FF00}   % = 128.255.0

%% >> subject colors ---------------------------------------------------------%%
\definecolor{chemie}{HTML}{004080}      % = 0.64.128
\definecolor{biologie}{HTML}{008040}    % = 0.128.64
\definecolor{physik}{HTML}{C00000}      % = 192.0.0

%% >>> color text, useful for corrections
\newcommand{\gru}[1]{\textcolor{green}{#1}}
\newcommand{\rot}[1]{\textcolor{red}{#1}}

%% > Other ================================================================== %%
\DeclareMathOperator{\exfu}{e}          % Euler constant
\DeclareMathOperator{\im}{i}            % imaginary unit
\DeclareMathOperator{\reteil}{Re}       % real part
\DeclareMathOperator{\imteil}{Im}       % imaginary part
\DeclareMathOperator{\sgn}{sgn}         % signum function
\DeclareSIUnit{\molar}{M}
%% >>> first derivative
\newcommand{\dif}[2][]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}#2}}}
%% >>> second derivative
\newcommand{\ddif}[2][]{\ensuremath{\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}}}
%% >>> third derivative
\newcommand{\dddif}[2][]{\ensuremath{\frac{\mathrm{d}^3#1}{\mathrm{d}#2^3}}}
%% >>> double arrow (follows)
\newcommand{\also}{\ensuremath{\Rightarrow}}
%% >>> single arrow
\newcommand{\dann}{\ensuremath{\longrightarrow}}
%% >>> corresponds-to sign
\newcommand{\oder}{\ensuremath{\widehat{=}}}
%% >>> golden ration phi
\newcommand{\golden}{\textrm{\straightphi}}
%% >>> density symbol
\newcommand{\dichte}{\ensuremath\varrho}

%% > Error ================================================================== %%
\DeclareDefaultOption{\PackageWarning{momokit}{Unknown option '\CurrentOption'}}
\endinput
%% ========================================================================== %%
%%  Changelog
%%  ==========
%%  Unreleased
%%  ----------
%%  - 2019-07-05: adapted to 80 columns per line limit
%%  
%%  Version 2.2
%%  ----------
%%  - 2018-06-15: added `acro` package
%%  - 2018-06-14: renamed momo.sty to momokit.sty
%%  - removed scrhack
%%  
%%  Version 2.1
%%  ----------
%%  - moved hyperref to the end of the packages
%%  - removed subfiles package
%%  
%%  Version 2.0
%%  ----------
%%  - started changelogging
%%  - reworked momo.sty to be included in all the classes
%%  - derivative command changed
%%  - captions changed
%%  - cleaned up
%% ========================================================================== %%